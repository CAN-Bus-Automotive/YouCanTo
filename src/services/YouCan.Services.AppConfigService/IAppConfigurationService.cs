﻿namespace YouCan.Services.AppConfigService
{
    /// <summary>
    /// Provide service to work with application's configuration.
    /// </summary>
    public interface IAppConfigurationService
    {
        AppConfig Configuration { get; set; }

        AppConfig ConfigurationBehind { get; set; }
    }
}
