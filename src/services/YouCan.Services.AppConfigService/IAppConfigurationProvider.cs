﻿using YouCan.Services.AppConfigService.Utility;

namespace YouCan.Services.AppConfigService
{
    public interface IAppConfigurationProvider
    {
        event TypedEventHandler<IAppConfigurationProvider> ConfigurationSaved;

        ConfigurationModel Configuration { get; }

        void LoadSettingsFromStorage();

        void SaveChanges();
    }
}
