﻿using System.Runtime.Serialization;
using Avalonia;
using Avalonia.Controls;
using ReactiveUI;

namespace YouCan.Services.AppConfigService
{
    public sealed class AppConfig : ReactiveObject
    {
        private int _windowHeight = 400;
        private int _windowWidth = 800;
        private double _signalSectionWidth = 200;
        private double _msgSectionWidth = 350;
        private WindowState _mainWindowState = WindowState.Normal;
        private PixelPoint _mainWindowPosition = new PixelPoint(20, 20);

        [DataMember]
        public int WindowHeight
        {
            get => _windowHeight;
            set => this.RaiseAndSetIfChanged(ref _windowHeight, value);
        }

        [DataMember]
        public int WindowWidth
        {
            get => _windowWidth;
            set => this.RaiseAndSetIfChanged(ref _windowWidth, value);
        }

        [DataMember]
        public WindowState MainWindowState
        {
            get => _mainWindowState;
            set => this.RaiseAndSetIfChanged(ref _mainWindowState, value);
        }

        [DataMember]
        public PixelPoint MainWindowPosition
        {
            get => _mainWindowPosition;
            set => this.RaiseAndSetIfChanged(ref _mainWindowPosition, value);
        }

        [DataMember]
        public double CommandSectionWidth
        {
            get => _signalSectionWidth;
            set => this.RaiseAndSetIfChanged(ref _signalSectionWidth, value);
        }

        [DataMember]
        public double MsgSectionWidth
        {
            get => _msgSectionWidth;
            set => this.RaiseAndSetIfChanged(ref _msgSectionWidth, value);
        }
    }
}
