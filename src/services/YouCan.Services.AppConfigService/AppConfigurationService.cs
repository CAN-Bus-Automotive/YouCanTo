﻿namespace YouCan.Services.AppConfigService
{
    /// <inheritdoc/>
    public class AppConfigurationService : IAppConfigurationService
    {
        public AppConfigurationService()
        {
            Configuration = new AppConfig();
            ConfigurationBehind = new AppConfig();
        }

        public AppConfig Configuration { get; set; }

        public AppConfig ConfigurationBehind { get; set; }
    }
}
