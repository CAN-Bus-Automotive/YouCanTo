﻿using System;
using System.Collections.Generic;

namespace YouCan.Services.DbcService.Impl
{
    public class DbcCommand
    {
        public DbcCommand(string name, int index, int id, int dcl, List<byte> body)
        {
            Name = name;
            Index = index;
            Id = id;
            Dcl = dcl;
            Body = body ?? new List<byte>(Dcl);

            ValidateInitialization();
        }

        public string Name { get; }

        public int Index { get; set; }

        public int Id { get; }

        public int Dcl { get; }

        public List<byte> Body { get; }

        private void ValidateInitialization()
        {
            if (Dcl > 8)
            {
                throw new IndexOutOfRangeException("Dcl must be in range: [0 .. 8]");
            }

            if (!(Index == 1 || Index == 2))
            {
                throw new IndexOutOfRangeException("Index must be: 1 or 2");
            }
        }
    }
}
