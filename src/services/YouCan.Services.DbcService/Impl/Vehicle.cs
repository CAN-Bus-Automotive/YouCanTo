﻿namespace YouCan.Services.DbcService.Impl
{
    public class Vehicle
    {
        public string Model { get; set; }

        public string EngineType { get; set; }

        public string TransmissionType { get; set; }

        public int ModelYear { get; set; }
    }
}
