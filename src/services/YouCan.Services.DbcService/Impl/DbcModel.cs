﻿using System;
using System.Collections.ObjectModel;

namespace YouCan.Services.DbcService.Impl
{
    public class DbcModel
    {
        public DbcModel()
        {
            DbcMessages = new ObservableCollection<DbcMessage>();
            DbcCommands = new ObservableCollection<DbcCommand>();
            DbcKeepAliveCommands = new ObservableCollection<DbcKeepAliveCommand>();
        }

        public string DbcVersion { get; set; }

        public DateTime CreationDate { get; set; }

        public Vehicle Vehicle { get; set; }

        public ObservableCollection<DbcMessage> DbcMessages { get; set; }

        public ObservableCollection<DbcCommand> DbcCommands { get; set; }

        public ObservableCollection<DbcKeepAliveCommand> DbcKeepAliveCommands { get; set; }
    }
}
