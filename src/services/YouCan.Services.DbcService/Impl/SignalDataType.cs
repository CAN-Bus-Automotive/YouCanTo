﻿namespace YouCan.Services.DbcService.Impl
{
    public enum SignalDataType
    {
        Int,
        Uint,
        Bool,
        Byte
    }
}
