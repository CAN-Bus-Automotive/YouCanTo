﻿using System.Collections.Generic;

namespace YouCan.Services.DbcService.Impl
{
    public class DbcKeepAliveCommand : DbcCommand
    {
        public DbcKeepAliveCommand(
            string name,
            int index,
            int id,
            int dcl,
            List<byte> body,
            int period)
            : base(name, index, id, dcl, body)
        {
            Period = period;
        }

        public int Period { get; set; }
    }
}
