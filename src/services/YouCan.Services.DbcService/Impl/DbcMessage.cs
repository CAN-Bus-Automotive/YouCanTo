﻿using System.Collections.ObjectModel;

namespace YouCan.Services.DbcService.Impl
{
    public class DbcMessage
    {
        public DbcMessage()
        {
            DbcSignals = new ObservableCollection<DbcSignal>();
        }

        public string Type { get; set; }

        public string Sender { get; set; }

        public ObservableCollection<DbcSignal> DbcSignals { get; set; }
    }
}
