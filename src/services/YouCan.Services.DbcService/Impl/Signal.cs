﻿namespace YouCan.Services.DbcService.Impl
{
    public class DbcSignal
    {
        public string Name { get; set; }

        public int BusIndex { get; set; }

        public int MId { get; set; }

        public int MDcl { get; set; }

        public SignalDataType SignalDataType { get; set; }

        public int ByteIndex { get; set; }

        public int StartBit { get; set; }

        public int StopBit { get; set; }

        public int Length { get; set; }

        public int MinValue { get; set; }

        public int MaxValue { get; set; }

        public int Offset { get; set; }

        public ByteOrder ByteOrder { get; set; }

        public int Scale { get; set; }

        public string Unit { get; set; }

        public string Note { get; set; }
    }
}
