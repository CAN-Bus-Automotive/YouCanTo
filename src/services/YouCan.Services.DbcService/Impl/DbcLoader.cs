﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using YouCan.Services.DbcService.Exceptions;

namespace YouCan.Services.DbcService.Impl
{
    public class DbcLoader : IDbcLoader
    {
        private readonly JsonSerializer _jsonSerializer;

        public DbcLoader()
        {
            _jsonSerializer = new JsonSerializer()
            {
                NullValueHandling = NullValueHandling.Include,
                Formatting = Formatting.Indented
            };
        }

        /// <inheritdoc/>
        public Task<DbcModel> LoadDbcModel(string pathToModel)
        {
            try
            {
                // deserialize JSON directly from a file
                using (StreamReader file = File.OpenText(pathToModel))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    DbcModel dcbModel = (DbcModel)serializer.Deserialize(file, typeof(DbcModel));

                    return Task.FromResult(dcbModel);
                }
            }
            catch (Exception e)
            {
                throw new DbcServiceException(e.StackTrace);
            }
        }

        /// <inheritdoc/>
        public Task SaveDbcModel(string pathToModel, DbcModel dbcModel)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(pathToModel))
                using (JsonWriter jw = new JsonTextWriter(sw))
                {
                    _jsonSerializer.Serialize(jw, dbcModel);
                    return Task.CompletedTask;
                }
            }
            catch (Exception e)
            {
                throw new DbcServiceException(e.StackTrace);
            }
        }
    }
}
