﻿using System.Threading.Tasks;
using YouCan.Services.DbcService.Exceptions;
using YouCan.Services.DbcService.Impl;

namespace YouCan.Services.DbcService
{
    /// <summary>
    /// Present service to work with DbcModel.
    /// </summary>
    public interface IDbcLoader
    {
        /// <summary>
        /// Load DBC model from any source py path.
        /// </summary>
        /// <param name="pathToModel">Path to file with DbcModel.</param>
        /// <returns>DbcModel.</returns>
        /// <exception cref="DbcServiceException">Throw exception if error.</exception>
        Task<DbcModel> LoadDbcModel(string pathToModel);

        /// <summary>
        /// Save DBC model from any source py path.
        /// </summary>
        /// <param name="pathToModel">Path to file with DbcModel.</param>
        /// <param name="dbcModel">DbcModel model instance.</param>
        /// <returns><see cref="Task"/>.</returns>
        /// <exception cref="DbcServiceException">Throw exception if error.</exception>
        Task SaveDbcModel(string pathToModel, DbcModel dbcModel);
    }
}
