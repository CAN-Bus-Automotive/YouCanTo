﻿using System;
using System.Runtime.Serialization;
using JetBrains.Annotations;

namespace YouCan.Services.DbcService.Exceptions
{
    public class DbcServiceException : Exception
    {
        public DbcServiceException()
        {
        }

        protected DbcServiceException([NotNull] SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public DbcServiceException(string message)
            : base(message)
        {
        }

        public DbcServiceException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
