﻿using System;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Threading;
using System.Threading.Tasks;

namespace YouCan.Servies.UpdaterChacker
{
    public class NetUpdater
    {
        public const string DefaultUrlToRemoteVersion = @"https://gitlab.com/CAN-Bus-Automotive/YouCanTo/-/jobs/artifacts/feature/clear_papper/raw/YouCanTo/version.txt?job=package_job_win_x64";
        public const string DefaultUrlToUpdateServer = @"https://gitlab.com/CAN-Bus-Automotive/YouCanTo/-/jobs/artifacts/feature/clear_papper/download?job=package_job_win_x64";
        public const string DefaultPathToDownload = @"";
        public const string UpdateInfoFile = @"version.txt";
        private TimeSpan _defaultCheckingInterval = TimeSpan.FromSeconds(5);

        public event EventHandler<bool> OnUpdateAvailable;

        public void StartUpdateCheckTask(
            string urlToSource,
            TimeSpan checkingInterval,
            CancellationToken cancellationToken)
        {
            Task.Run(async () =>
             {
                 while (!cancellationToken.IsCancellationRequested)
                 {
                     await Task.Delay(_defaultCheckingInterval, cancellationToken).ConfigureAwait(false);
                     CheckForUpdate(urlToSource, cancellationToken);

                     var currentVersion = await GetCurrentVersion();
                     var updateVersion = await GetUpdateVersion();

                     OnUpdateAvailable?.Invoke(this, currentVersion != updateVersion);
                     await Task.Delay(checkingInterval, cancellationToken).ConfigureAwait(false);
                 }
             });
        }

        public void CheckForUpdate(
            string urlToUpdateServer,
            CancellationToken cancellationToken)
        {
            var rootFolder = Directory.GetCurrentDirectory();
            var tempPath = Path.Combine(rootFolder, $@"temp\{UpdateInfoFile}");
            var wc = new WebClient();
            try
            {
                wc.DownloadFile(new Uri(DefaultUrlToRemoteVersion), tempPath);
            }
            catch (Exception ex)
            {
                throw new UpdateException($"Update error: {ex.Message}", ex);
            }
            finally
            {
                wc.Dispose();
            }
        }

        public async Task<string> GetCurrentVersion()
        {
            string rootFolder = Directory.GetCurrentDirectory();
            var pathToUpdateInfoFile = Path.Combine(rootFolder, UpdateInfoFile);

            try
            {
                using (var reader = File.OpenText(pathToUpdateInfoFile))
                {
                    var fileText = await reader.ReadToEndAsync().ConfigureAwait(false);
                    return fileText;
                }
            }
            catch (Exception ex)
            {
                throw new UpdateException(ex.Message, ex);
            }
        }

        public async Task<string> GetUpdateVersion()
        {
            string rootFolder = Directory.GetCurrentDirectory();
            var pathToUpdateInfoFile = Path.Combine(rootFolder, $@"temp\{UpdateInfoFile}");

            try
            {
                using (var reader = File.OpenText(pathToUpdateInfoFile))
                {
                    var fileText = await reader.ReadToEndAsync().ConfigureAwait(false);
                    return fileText;
                }
            }
            catch (Exception ex)
            {
                throw new UpdateException(ex.Message, ex);
            }
        }

        public void DownloadLatestUpdate(string path)
        {
            var wc = new WebClient();
            wc.OpenRead(new Uri(DefaultUrlToUpdateServer));

            string headerContentDisposition = wc.ResponseHeaders["content-disposition"];
            string fileName = new ContentDisposition(headerContentDisposition).FileName;
            string fullFileLocation = Path.Combine(path, fileName);

            wc.DownloadFileAsync(new Uri(DefaultUrlToUpdateServer), fullFileLocation);
        }
    }
}
