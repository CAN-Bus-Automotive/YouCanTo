﻿using AutoMapper;
using YouCan.Services.DbcService.Impl;
using YouCan.Ui.Avalonia.Models;

namespace YouCan.Ui.Avalonia.Helpers
{
    public class AutoMapperProfile
    {
        public void Initialize(IMapperConfigurationExpression configuration)
        {
            configuration.CreateMap<DbcMessage, ObservableDbcMessage>()
                .ForMember(dest => dest.Type, act => act.MapFrom(src => src.Type))
                .ForMember(dest => dest.Sender, act => act.MapFrom(src => src.Sender))
                .ForMember(dest => dest.ObservableDbcSignals, act => act.MapFrom(src => src.DbcSignals));

            configuration.CreateMap<DbcSignal, ObservableDbcSignal>()
                .ForMember(dest => dest.BusIndex, act => act.MapFrom(src => src.BusIndex))
                .ForMember(dest => dest.ByteIndex, act => act.MapFrom(src => src.ByteIndex))
                .ForMember(dest => dest.ByteOrder, act => act.MapFrom(src => src.ByteOrder))
                .ForMember(dest => dest.CalculatedValue, act => act.Ignore())
                .ForMember(dest => dest.Length, act => act.MapFrom(src => src.Length))
                .ForMember(dest => dest.MDcl, act => act.MapFrom(src => src.MDcl))
                .ForMember(dest => dest.MId, act => act.MapFrom(src => src.MId))
                .ForMember(dest => dest.MaxValue, act => act.MapFrom(src => src.MaxValue))
                .ForMember(dest => dest.MinValue, act => act.MapFrom(src => src.MinValue))
                .ForMember(dest => dest.Name, act => act.MapFrom(src => src.Name))
                .ForMember(dest => dest.Note, act => act.MapFrom(src => src.Note))
                .ForMember(dest => dest.Offset, act => act.MapFrom(src => src.Offset))
                .ForMember(dest => dest.ByteOrder, act => act.MapFrom(src => src.ByteOrder))
                .ForMember(dest => dest.SignalDataType, act => act.MapFrom(src => src.SignalDataType))
                .ForMember(dest => dest.Scale, act => act.MapFrom(src => src.Scale))
                .ForMember(dest => dest.Unit, act => act.MapFrom(src => src.Unit));

            configuration.CreateMap<DbcCommand, ObservableDbcCommand>()
                .ForMember(dest => dest.DbcCommand, act => act.MapFrom(src => src));

            configuration.CreateMap<DbcKeepAliveCommand, ObservableDbcKeepAliveCommand>()
                .ForMember(dest => dest.KeepAliveCommand, act => act.MapFrom(src => src))
                .ForMember(dest => dest.IsActive, act => act.Ignore());
        }
    }
}
