﻿using System;
using System.Globalization;
using Avalonia.Controls;
using Avalonia.Data.Converters;

namespace YouCan.Ui.Avalonia.Converters
{
    public class GridLengthValueConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return new GridLength((double)value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            GridLength gridLength = (GridLength)value;

            if (gridLength != null)
            {
                return gridLength.Value;
            }
            else
            {
                throw new InvalidCastException("Value is not GridLength type");
            }
        }
    }
}
