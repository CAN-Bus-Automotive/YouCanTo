﻿using Splat;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia
{
    public class ViewModelLocator
    {
        public readonly bool IsDesignMode = false;

        public ViewModelLocator()
        {
        }

        public MainWindowViewModel MainWindowViewModel => Locator.Current.GetService<MainWindowViewModel>();

        public DbcViewModel DbcViewModel => Locator.Current.GetService<DbcViewModel>();

        public AboutViewModel AboutViewModel => Locator.Current.GetService<AboutViewModel>();

        public MainMenuViewModel MainMenuViewModel => Locator.Current.GetService<MainMenuViewModel>();

        public SetupViewModel SetupViewModel => Locator.Current.GetService<SetupViewModel>();

        public TraceViewModel TraceViewModel => Locator.Current.GetService<TraceViewModel>();
    }
}
