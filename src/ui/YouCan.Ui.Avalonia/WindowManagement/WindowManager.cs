﻿using System.Threading.Tasks;
using Avalonia.Controls;
using YouCan.Ui.Avalonia.Views;

namespace YouCan.Ui.Avalonia.WindowManagement
{
    public class WindowManager : WindowManagerBase
    {
        private static readonly object SyncObj = new object();
        private static WindowManager _instance = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="WindowManager"/> class.
        /// </summary>
        private WindowManager()
        {
        }

        /// <summary>
        /// Return singleton instance of the <see cref="WindowManager"/> class.
        /// </summary>
        public static WindowManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncObj)
                    {
                        _instance = new WindowManager();
                    }
                }

                return _instance;
            }
        }

        public Task ShowAboutWindow()
        {
            var aboutWindow = new AboutWindow();
            aboutWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;

            return aboutWindow.ShowDialog(GetMainWindow());
        }

        public Task<string[]> ShowOpenFileDialog()
        {
            var fileDialog = new OpenFileDialog();
            var fileDialogResult = fileDialog.ShowAsync(GetMainWindow());

            return fileDialogResult;
        }

        public Task<string> ShowSaveFileDialog()
        {
            var fileDialog = new SaveFileDialog();
            var fileDialogResult = fileDialog.ShowAsync(GetMainWindow());

            return fileDialogResult;
        }

        public Task<string> ShowFolderDialog()
        {
            var folderDialog = new OpenFolderDialog();
            var folderDialogResult = folderDialog.ShowAsync(GetMainWindow());

            return folderDialogResult;
        }
    }
}
