﻿using System.Threading.Tasks;

namespace YouCan.Ui.Avalonia.WindowManagement.ProgressBox
{
    public class ProgressBoxManager : WindowManagerBase
    {
        private static readonly object SyncObj = new object();
        private static ProgressBoxManager _instance = null;
        private ProgressBoxWindow _progressBoxWindow;

        public static ProgressBoxManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncObj)
                    {
                        _instance = new ProgressBoxManager();
                    }
                }

                return _instance;
            }
        }

        public void ShowIndeterminateProgress()
        {
            _progressBoxWindow = new ProgressBoxWindow();
            _progressBoxWindow.ShowDialog(GetMainWindow());
        }

        public void HideProgress()
        {
            _progressBoxWindow?.Close();
        }
    }
}
