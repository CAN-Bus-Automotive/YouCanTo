using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;

namespace YouCan.Ui.Avalonia.WindowManagement
{
    public class WindowManagerBase
    {
        /// <summary>
        /// Return Main Windows of application.
        /// </summary>
        /// <returns>Window <see cref="Window"/>.</returns>
        /// <exception cref="Exception">If window not found.</exception>
        protected Window GetMainWindow()
        {
            if (Application.Current.ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktopLifetime)
            {
                return desktopLifetime.MainWindow;
            }

            throw new Exception("Main Window not found");
        }
    }
}
