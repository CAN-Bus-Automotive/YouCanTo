﻿using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Media;

namespace YouCan.Ui.Avalonia.WindowManagement.MessageBox
{
    /// <summary>
    /// Show and handle message boxes.
    /// </summary>
    public class MessageBoxManager : WindowManagerBase
    {
        private static readonly object SyncObj = new object();
        private static MessageBoxManager _instance = null;

        /// <summary>
        /// MessageBox thread safe instance.
        /// </summary>
        public static MessageBoxManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (SyncObj)
                    {
                        _instance = new MessageBoxManager();
                    }
                }

                return _instance;
            }
        }

        public Task<bool> ShowError(string title, string message)
        {
            var messageBoxWindow = new MessageBoxWindow();

            messageBoxWindow.Title = "Error";
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Foreground = Brushes.Red;
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Text = title;
            messageBoxWindow.FindControl<TextBox>("MessageBody").Text = message;
            messageBoxWindow.FindControl<Button>("OkButton").IsVisible = true;
            messageBoxWindow.FindControl<Button>("CancelButton").IsVisible = false;

            return messageBoxWindow.ShowDialog<bool>(GetMainWindow());
        }

        public Task<bool> ShowInfo(string title, string message)
        {
            var messageBoxWindow = new MessageBoxWindow();

            messageBoxWindow.Title = "Information";
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Foreground = Brushes.DarkBlue;
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Text = title;
            messageBoxWindow.FindControl<TextBox>("MessageBody").Text = message;
            messageBoxWindow.FindControl<Button>("OkButton").IsVisible = true;
            messageBoxWindow.FindControl<Button>("CancelButton").IsVisible = false;

            return messageBoxWindow.ShowDialog<bool>(GetMainWindow());
        }

        public Task<bool> ShowWarning(string title, string message)
        {
            var messageBoxWindow = new MessageBoxWindow();

            messageBoxWindow.Title = "Warning";
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Foreground = Brushes.OrangeRed;
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Text = title;
            messageBoxWindow.FindControl<TextBox>("MessageBody").Text = message;
            messageBoxWindow.FindControl<Button>("OkButton").IsVisible = true;
            messageBoxWindow.FindControl<Button>("CancelButton").IsVisible = true;

            return messageBoxWindow.ShowDialog<bool>(GetMainWindow());
        }

        public Task<bool> ShowQuestion(string title, string message)
        {
            var messageBoxWindow = new MessageBoxWindow();

            messageBoxWindow.Title = "Question";
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Foreground = Brushes.Black;
            messageBoxWindow.FindControl<TextBlock>("MessageHeader").Text = title + " ?";
            messageBoxWindow.FindControl<TextBox>("MessageBody").Text = message;
            messageBoxWindow.FindControl<Button>("OkButton").IsVisible = true;
            messageBoxWindow.FindControl<Button>("CancelButton").IsVisible = true;

            return messageBoxWindow.ShowDialog<bool>(GetMainWindow());
        }
    }
}
