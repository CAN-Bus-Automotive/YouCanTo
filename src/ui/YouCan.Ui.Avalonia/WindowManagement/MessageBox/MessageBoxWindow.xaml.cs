﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace YouCan.Ui.Avalonia.WindowManagement.MessageBox
{
    internal class MessageBoxWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MessageBoxWindow"/> class.
        /// </summary>
        public MessageBoxWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            AvaloniaXamlLoader.Load(this);

            this.FindControl<Button>("OkButton").Click += OkButtonClick;
            this.FindControl<Button>("CancelButton").Click += CancelButtonClick;
        }

        private void OkButtonClick(object sender, RoutedEventArgs e)
        {
            Close(true);
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close(false);
        }
    }
}
