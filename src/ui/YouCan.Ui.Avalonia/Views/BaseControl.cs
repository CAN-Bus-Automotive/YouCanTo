﻿using System.Reactive.Disposables;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace YouCan.Ui.Avalonia.Views
{
    /// <summary>
    /// Workaround for model activation.
    /// </summary>
    public class BaseControl<TViewModel> : ReactiveUserControl<TViewModel>
        where TViewModel : class
    {
        public BaseControl(bool activate = true)
        {
            ApplyDataContext();

            if (activate)
            {
                this.WhenActivated(disposables =>
                {
                    Disposable.Create(() => { }).DisposeWith(disposables);
                });
            }
        }

        private void ApplyDataContext()
        {
            object obj;
            App.Current.Resources.TryGetResource("Locator", out obj);

            ViewModelLocator vmLocator = obj as ViewModelLocator;

            if (vmLocator != null)
            {
                DataContext = vmLocator;
            }
            else
            {
                throw new LocatorNotFoundException();
            }
        }
    }
}
