﻿using Avalonia.Markup.Xaml;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Views
{
    public class SetupView : BaseControl<SetupViewModel>
    {
        public SetupView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
