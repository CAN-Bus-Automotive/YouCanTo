﻿using Avalonia.Markup.Xaml;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Views
{
    public class TraceView : BaseControl<TraceViewModel>
    {
        public TraceView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
           AvaloniaXamlLoader.Load(this);
        }
    }
}
