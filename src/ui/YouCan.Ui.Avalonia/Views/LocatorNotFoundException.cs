﻿using System;

namespace YouCan.Ui.Avalonia.Views
{
    public class LocatorNotFoundException : Exception
    {
        public LocatorNotFoundException()
            : base()
        {
        }

        public LocatorNotFoundException(string message)
            : base(message)
        {
        }

        public LocatorNotFoundException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
