﻿using Avalonia.Markup.Xaml;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Views
{
    public class MainMenuView : BaseControl<MainWindowViewModel>
    {
        public MainMenuView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
