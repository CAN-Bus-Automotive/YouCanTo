﻿using Avalonia;
using Avalonia.Markup.Xaml;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Views
{
    public class AboutWindow : BaseWindow<AboutViewModel>
    {
        public AboutWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
