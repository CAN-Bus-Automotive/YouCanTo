﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace YouCan.Ui.Avalonia.Views.Components.DbcMessages
{
    public class DbcMessageItem : UserControl
    {
        public DbcMessageItem()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
