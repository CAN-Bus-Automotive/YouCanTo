﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace YouCan.Ui.Avalonia.Views.Components.DbcCommnds
{
    public class DbcCommndItem : UserControl
    {
        public DbcCommndItem()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
