﻿using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace YouCan.Ui.Avalonia.Views.Components.DbcCommnds
{
    public class DbcKeepAliaveCommandItem : UserControl
    {
        public DbcKeepAliaveCommandItem()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
