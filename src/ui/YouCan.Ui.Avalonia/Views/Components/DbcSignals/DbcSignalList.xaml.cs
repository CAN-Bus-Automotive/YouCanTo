﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace YouCan.Ui.Avalonia.Views.Components.DbcSignals
{
    public class DbcSignalList : UserControl
    {
        public DbcSignalList()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
