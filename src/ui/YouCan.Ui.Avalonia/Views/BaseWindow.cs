﻿using System.Reactive.Disposables;
using Avalonia.ReactiveUI;
using ReactiveUI;

namespace YouCan.Ui.Avalonia.Views
{
    /// <summary>
    /// Workaround for model activation.
    /// </summary>
    public class BaseWindow<TViewModel> : ReactiveWindow<TViewModel>
        where TViewModel : class
    {
        public BaseWindow(bool activate = true)
        {
            ApplyDataContext();

            if (activate)
            {
                this.WhenActivated(disposables =>
                {
                    Disposable.Create(() => { }).DisposeWith(disposables);
                });
            }
        }

        private void ApplyDataContext()
        {
            object obj;
            App.Current.Resources.TryGetResource("Locator", out obj);

            ViewModelLocator vmLocator = obj as ViewModelLocator;

            if (vmLocator != null)
            {
                DataContext = vmLocator;
            }
            else
            {
                throw new LocatorNotFoundException();
            }
        }
    }
}
