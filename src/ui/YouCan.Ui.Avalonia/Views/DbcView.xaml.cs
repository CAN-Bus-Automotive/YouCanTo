﻿using Avalonia.Markup.Xaml;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Views
{
    public class DbcView : BaseControl<DbcViewModel>
    {
        public DbcView()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
           AvaloniaXamlLoader.Load(this);
        }
    }
}
