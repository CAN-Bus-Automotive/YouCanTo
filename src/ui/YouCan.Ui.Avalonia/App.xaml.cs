﻿using System;
using AutoMapper;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;
using ReactiveUI;
using Splat;
using YouCan.Services.AppConfigService;
using YouCan.Ui.Avalonia.Helpers;
using YouCan.Ui.Avalonia.ViewModels;
using YouCan.Ui.Avalonia.Views;

namespace YouCan.Ui.Avalonia
{
    /// <summary>
    /// Tha main application's class.
    /// </summary>
    public class App : Application
    {
        /// <inheritdoc/>
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void InitializeAutomapper()
        {
            try
            {
                Mapper.Configuration.AssertConfigurationIsValid();
            }
            catch (Exception)
            {
                Mapper.Initialize(cfg => new AutoMapperProfile().Initialize(cfg));
            }
        }

        // Your application's entry point. Here you can initialize your MVVM framework, DI
        // container, etc.

        /// <inheritdoc/>
        public override void OnFrameworkInitializationCompleted()
        {
            InitializeAutomapper();

            // Init container before app state reading.
            // Function call order is important.
            AppConfigurationService appConfig = null;

            // This check fix previewer crash.
            if (!Design.IsDesignMode)
            {
                appConfig = RestoreAppState();
            }

            Locator.CurrentMutable.AddConfig(appConfig);
            Locator.CurrentMutable.AddServices();
            Locator.CurrentMutable.AddViewModels();
            Locator.CurrentMutable.AddViews(new MainWindowViewModel());

            // Init Main window
            var window = new MainWindow()
            {
                DataContext = Locator.Current.GetService<MainWindowViewModel>()
            };

            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = window;
            }

            base.OnFrameworkInitializationCompleted();
        }

        private AppConfigurationService RestoreAppState()
        {
            var suspension = new AutoSuspendHelper(ApplicationLifetime);
            RxApp.SuspensionHost.CreateNewAppState = () => new AppConfigurationService();
            RxApp.SuspensionHost.SetupDefaultSuspendResume(new JsonSuspensionDriver("appState.json"));
            suspension.OnFrameworkInitializationCompleted();
            var state = RxApp.SuspensionHost.GetAppState<AppConfigurationService>();
            return state;
        }
    }
}
