﻿using ReactiveUI;
using Splat;
using YouCan.Services.AppConfigService;
using YouCan.Services.DbcService;
using YouCan.Services.DbcService.Impl;
using YouCan.Servies.UpdaterChacker;
using YouCan.Ui.Avalonia.ViewModels;
using YouCan.Ui.Avalonia.Views;

namespace YouCan.Ui.Avalonia
{
    /// <summary>
    /// Register all dependencies (Services, ViewModels and so on).
    /// </summary>
    public static class Registry
    {
        public static void AddConfig(this IMutableDependencyResolver container, IAppConfigurationService config)
        {
            container.RegisterLazySingleton<IAppConfigurationService>(() => config);
        }

        public static void AddServices(this IMutableDependencyResolver container)
        {
            container.RegisterLazySingleton<IDbcLoader>(() => new DbcLoader());
            container.RegisterLazySingleton<NetUpdater>(() => new NetUpdater());
        }

        public static void AddViewModels(this IMutableDependencyResolver container)
        {
            container.RegisterLazySingleton<TraceViewModel>(() => new TraceViewModel());
            container.RegisterLazySingleton<DbcViewModel>(() => new DbcViewModel());
            container.RegisterLazySingleton<AboutViewModel>(() => new AboutViewModel());
            container.RegisterLazySingleton<SetupViewModel>(() => new SetupViewModel());
        }

        public static void AddViews(this IMutableDependencyResolver container, MainWindowViewModel mainScreen)
        {
            container.RegisterConstant<MainWindowViewModel>(mainScreen);
            container.RegisterLazySingleton<IViewFor<DbcViewModel>>(() => new DbcView());
            container.RegisterLazySingleton<IViewFor<TraceViewModel>>(() => new TraceView());
            container.RegisterLazySingleton<IViewFor<SetupViewModel>>(() => new SetupView());
        }
    }
}
