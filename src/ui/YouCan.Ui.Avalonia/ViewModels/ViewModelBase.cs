﻿using ReactiveUI;

namespace YouCan.Ui.Avalonia.ViewModels
{
    public class ViewModelBase : ReactiveObject, ISupportsActivation
    {
        public ViewModelActivator Activator => new ViewModelActivator();
    }
}
