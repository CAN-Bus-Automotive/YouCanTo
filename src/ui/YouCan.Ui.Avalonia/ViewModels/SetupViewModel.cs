﻿using ReactiveUI;

namespace YouCan.Ui.Avalonia.ViewModels
{
    public class SetupViewModel : ViewModelBase, IRoutableViewModel
    {
        public string UrlPathSegment { get; }

        public IScreen HostScreen { get; }
    }
}
