﻿using ReactiveUI;
using Splat;

namespace YouCan.Ui.Avalonia.ViewModels
{
    public class TraceViewModel : ViewModelBase, IRoutableViewModel
    {
        public TraceViewModel()
        {
            HostScreen = Locator.Current.GetService<MainWindowViewModel>();
        }

        public string UrlPathSegment => "/trace";

        public IScreen HostScreen { get; }
    }
}
