﻿using System;
using System.Reactive;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Avalonia.Controls;
using ReactiveUI;
using Splat;
using YouCan.Services.AppConfigService;
using YouCan.Services.DbcService;
using YouCan.Servies.UpdaterChacker;
using YouCan.Ui.Avalonia.WindowManagement;

namespace YouCan.Ui.Avalonia.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IScreen
    {
        private readonly ReactiveCommand<Unit, Unit> _downloadUpdateCmd;
        private readonly IDbcLoader _dbcLoader;
        private readonly NetUpdater _netUpdater;
        private readonly TraceViewModel _traceViewModel;
        private readonly SetupViewModel _setupViewModel;
        private RoutingState _router;
        private CancellationTokenSource _cts;
        private bool _isUpdateAvailable;

        public MainWindowViewModel(
            IAppConfigurationService configurationService = null,
            TraceViewModel traceViewModel = null,
            DbcViewModel dbcViewModel = null,
            SetupViewModel setupViewModel = null,
            IDbcLoader dbcLoader = null,
            NetUpdater netUpdater = null)
        {
            _router = new RoutingState();
            ConfigurationService = configurationService ?? Locator.Current.GetService<IAppConfigurationService>();
            _traceViewModel = traceViewModel ?? Locator.Current.GetService<TraceViewModel>();
            DbcViewModel = dbcViewModel ?? Locator.Current.GetService<DbcViewModel>();
            _setupViewModel = setupViewModel ?? Locator.Current.GetService<SetupViewModel>();
            _dbcLoader = dbcLoader ?? Locator.Current.GetService<IDbcLoader>();
            _netUpdater = netUpdater ?? Locator.Current.GetService<NetUpdater>();

            _downloadUpdateCmd = ReactiveCommand.CreateFromTask(async () =>
            {
                var openFolderDialog = new OpenFolderDialog();
                var folder = await WindowManager.Instance.ShowFolderDialog();

                if (!string.IsNullOrEmpty(folder))
                {
                    _netUpdater.DownloadLatestUpdate(folder);
                }
            });

            _cts = new CancellationTokenSource();

            _netUpdater.StartUpdateCheckTask(NetUpdater.DefaultUrlToUpdateServer, TimeSpan.FromMinutes(5), _cts.Token);
            _netUpdater.OnUpdateAvailable += OnUpdateAvailable;
        }

        private void OnUpdateAvailable(object sender, bool updateState)
        {
            IsUpdateAvailable = updateState;
        }

        public RoutingState Router
        {
            get => _router;
            set => this.RaiseAndSetIfChanged(ref _router, value);
        }

        public bool IsUpdateAvailable
        {
            get => _isUpdateAvailable;
            set => this.RaiseAndSetIfChanged(ref _isUpdateAvailable, value);
        }

        public DbcViewModel DbcViewModel { get; }

        public IAppConfigurationService ConfigurationService { get; }

        public ICommand DownloadUpdateCmd => _downloadUpdateCmd;

        public Task ShowAboutInfo()
        {
            return WindowManager.Instance.ShowAboutWindow();
        }
    }
}
