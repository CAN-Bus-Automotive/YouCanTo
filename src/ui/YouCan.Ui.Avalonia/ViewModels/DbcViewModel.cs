﻿using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using System.Windows.Input;
using AutoMapper;
using ReactiveUI;
using Splat;
using YouCan.Services.AppConfigService;
using YouCan.Services.DbcService;
using YouCan.Services.DbcService.Exceptions;
using YouCan.Services.DbcService.Impl;
using YouCan.Ui.Avalonia.Models;
using YouCan.Ui.Avalonia.WindowManagement;
using YouCan.Ui.Avalonia.WindowManagement.MessageBox;
using YouCan.Ui.Avalonia.WindowManagement.ProgressBox;

namespace YouCan.Ui.Avalonia.ViewModels
{
    public class DbcViewModel : ViewModelBase
    {
        private readonly ReactiveCommand<Unit, Unit> _refreshFromFileCmd;
        private readonly ReactiveCommand<Unit, Unit> _openDbcFileCmd;
        private readonly IDbcLoader _dbcLoader;
        private ObservableDbcMessage _selectedItem;
        private string _filePath;
        private DbcModel _dbcModel;

        public DbcViewModel(
            IAppConfigurationService configurationService = null,
            IDbcLoader dbcLoader = null)
        {
            ConfigurationService = configurationService ?? Locator.Current.GetService<IAppConfigurationService>();
            _dbcLoader = dbcLoader ?? Locator.Current.GetService<IDbcLoader>();

            ObservableDbcMessages = new ObservableCollection<ObservableDbcMessage>();
            ObservableDbcCommands = new ObservableCollection<ObservableDbcCommand>();
            ObservableDbcKeepAliveCommands = new ObservableCollection<ObservableDbcKeepAliveCommand>();

            var canRefresh = this
                .WhenAnyValue(vm => vm.DataModel)
                .Select(vm => vm?.DbcMessages.Count > 0);

            this.WhenAnyValue(vm => vm.DataModel)
                .Where(model => model?.DbcMessages.Count > 0)
                .Subscribe(model =>
                {
                    foreach (DbcMessage message in model.DbcMessages)
                    {
                        var obsMsg = Mapper.Map<ObservableDbcMessage>(message);
                        ObservableDbcMessages.Add(obsMsg);
                    }

                    foreach (DbcCommand dbcCmd in model.DbcCommands)
                    {
                        var obsDbcCmd = Mapper.Map<ObservableDbcCommand>(dbcCmd);
                        ObservableDbcCommands.Add(obsDbcCmd);
                    }

                    foreach (DbcKeepAliveCommand keepAliveCmd in model.DbcKeepAliveCommands)
                    {
                        var obsKeepAliveCmd = Mapper.Map<ObservableDbcKeepAliveCommand>(keepAliveCmd);
                        ObservableDbcKeepAliveCommands.Add(obsKeepAliveCmd);
                    }
                });

            _refreshFromFileCmd = ReactiveCommand.Create(() =>
                {
                    // TODO add some actions.
                }, canRefresh);

            _openDbcFileCmd = ReactiveCommand.CreateFromTask(async () =>
            {
                try
                {
                    var result = await WindowManager.Instance.ShowOpenFileDialog();
                    if (result.Length > 0)
                    {
                        ProgressBoxManager.Instance.ShowIndeterminateProgress();
                        DataModel = await _dbcLoader.LoadDbcModel(result[0]);
                        FilePath = result[0];
                    }
                }
                catch (DbcServiceException e)
                {
                    await MessageBoxManager.Instance.ShowError("Dbc file error", e.Message);
                    FilePath = string.Empty;
                }
                finally
                {
                    ProgressBoxManager.Instance.HideProgress();
                }
            });
        }

        public ICommand RefreshFromFileCmd => _refreshFromFileCmd;

        public ICommand OpenDbcFileCmd => _openDbcFileCmd;

        public ObservableDbcMessage SelectedItem
        {
            get => _selectedItem;
            set => this.RaiseAndSetIfChanged(ref _selectedItem, value);
        }

        public DbcModel DataModel
        {
            get => _dbcModel;
            set => this.RaiseAndSetIfChanged(ref _dbcModel, value);
        }

        public string FilePath
        {
            get => _filePath;
            set => this.RaiseAndSetIfChanged(ref _filePath, value);
        }

        public IAppConfigurationService ConfigurationService { get; }

        public ObservableCollection<ObservableDbcMessage> ObservableDbcMessages { get; set; }

        public ObservableCollection<ObservableDbcCommand> ObservableDbcCommands { get; set; }

        public ObservableCollection<ObservableDbcKeepAliveCommand> ObservableDbcKeepAliveCommands { get; set; }
    }
}
