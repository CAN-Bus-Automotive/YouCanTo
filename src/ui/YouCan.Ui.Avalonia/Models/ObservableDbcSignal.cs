﻿using ReactiveUI;
using YouCan.Services.DbcService.Impl;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Models
{
    public class ObservableDbcSignal : ViewModelBase
    {
        private string _calculatedValue = "No value";

        public string Name { get; set; }

        public int BusIndex { get; set; }

        public int MId { get; set; }

        public int MDcl { get; set; }

        public SignalDataType SignalDataType { get; set; } = SignalDataType.Uint;

        public int ByteIndex { get; set; }

        public int StartBit { get; set; }

        public int StopBit { get; set; }

        public int Length { get; set; }

        public int MinValue { get; set; }

        public int MaxValue { get; set; }

        public int Offset { get; set; }

        public ByteOrder ByteOrder { get; set; } = ByteOrder.Intel;

        public int Scale { get; set; }

        public string Unit { get; set; }

        public string Note { get; set; }

        public string CalculatedValue
        {
            get => _calculatedValue;
            set => this.RaiseAndSetIfChanged(ref _calculatedValue, value);
        }
    }
}
