﻿using System.Collections.ObjectModel;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Models
{
    public class ObservableDbcMessage : ViewModelBase
    {
        public ObservableDbcMessage()
        {
            ObservableDbcSignals = new ObservableCollection<ObservableDbcSignal>();
        }

        public string Type { get; set; }

        public string Sender { get; set; }

        public ObservableCollection<ObservableDbcSignal> ObservableDbcSignals { get; set; }
    }
}
