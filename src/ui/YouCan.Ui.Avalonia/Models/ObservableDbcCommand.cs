﻿using ReactiveUI;
using YouCan.Services.DbcService.Impl;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Models
{
    public class ObservableDbcCommand : ViewModelBase
    {
        private DbcCommand _dbcCommand;

        public DbcCommand DbcCommand
        {
            get => _dbcCommand;
            set => this.RaiseAndSetIfChanged(ref _dbcCommand, value);
        }
    }
}
