﻿using ReactiveUI;
using YouCan.Services.DbcService.Impl;
using YouCan.Ui.Avalonia.ViewModels;

namespace YouCan.Ui.Avalonia.Models
{
    public class ObservableDbcKeepAliveCommand : ViewModelBase
    {
        private DbcKeepAliveCommand _keepAliveCommand;
        private bool _isActive = false;

        public DbcKeepAliveCommand KeepAliveCommand
        {
            get => _keepAliveCommand;
            set => this.RaiseAndSetIfChanged(ref _keepAliveCommand, value);
        }

        public bool IsActive
        {
            get => _isActive;
            set => this.RaiseAndSetIfChanged(ref _isActive, value);
        }
    }
}
