﻿using System;
using System.Collections.ObjectModel;
using YouCan.Services.DbcService.Impl;

namespace YouCanToTests.UnitTests.Fakes
{
    public static class DbcFixture
    {
        public static DateTime TestDateTime => DateTime.Parse("2019/07/16");

        public static DbcModel TestDbcModel => new DbcModel()
        {
            Vehicle = new Vehicle()
            {
                EngineType = "Diesel",
                Model = "SantaFe",
                ModelYear = 2010,
                TransmissionType = "Automatic"
            },
            CreationDate = TestDateTime,
            DbcVersion = "0.1",
            DbcMessages = new ObservableCollection<DbcMessage>()
            {
                new DbcMessage()
                {
                    Type = "TestMessage",
                    Sender = "TestSender",
                    DbcSignals = new ObservableCollection<DbcSignal>()
                    {
                        new DbcSignal()
                        {
                            Name = "TestSignal",
                            BusIndex = 1,
                            MId = 0x00,
                            MDcl = 8,
                            ByteIndex = 0,
                            StartBit = 0,
                            StopBit = 8,
                            Length = 8,
                            Offset = 0,
                            Scale = 1,
                            SignalDataType = SignalDataType.Bool,
                            ByteOrder = ByteOrder.Intel,
                            MaxValue = 100,
                            MinValue = 0,
                            Unit = "Test Unit",
                            Note = "Test Note"
                        }
                    }
                }
            }
        };
    }
}
