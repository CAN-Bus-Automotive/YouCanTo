﻿using System.Collections.ObjectModel;
using AutoMapper;
using Xunit;
using YouCan.Services.DbcService.Impl;
using YouCan.Ui.Avalonia.Helpers;

namespace YouCanToTests.UnitTests.AutoMapper
{
    public class DbcMappingTests
    {
        private readonly DbcMessage _validDbcMessage;

        public DbcMappingTests()
        {
            _validDbcMessage = new DbcMessage
            {
                Type = "Ignition switch",
                Sender = "TestSender",
                DbcSignals = new ObservableCollection<DbcSignal>()
                {
                    new DbcSignal()
                    {
                        Name = "Ignition ON",
                        BusIndex = 1,
                        MId = 0x018,
                        MDcl = 8,
                        SignalDataType = SignalDataType.Byte,
                        ByteIndex = 2,
                        StartBit = 0,
                        StopBit = 1,
                        Length = 1,
                        MinValue = 0,
                        MaxValue = 1,
                        Offset = 0,
                        ByteOrder = ByteOrder.Motorola,
                        Scale = 1,
                        Unit = "On/Off",
                        Note = "TestNote"
                    }
                }
            };
        }

        [Fact]
        public void ConfigurationTests()
        {
            Mapper.Reset();
            Mapper.Initialize(cfg => new AutoMapperProfile().Initialize(cfg));

            Mapper.Configuration.AssertConfigurationIsValid();
        }
    }
}
