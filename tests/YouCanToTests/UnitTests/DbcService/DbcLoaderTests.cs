﻿using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Xunit;
using YouCan.Services.DbcService;
using YouCan.Services.DbcService.Impl;
using YouCanToTests.UnitTests.Fakes;

namespace YouCanToTests.UnitTests.DbcService
{
    public class DbcLoaderTests
    {
        private const string PathToActTestModel = "test_dbc_act.json";
        private const string PathToExpTestModel = "UnitTests/Assets/test_dbc_exp.json";
        private readonly IDbcLoader _dbcLoader;

        public DbcLoaderTests()
        {
            _dbcLoader = new DbcLoader();
        }

        [Fact]
        public async Task LoadDbcModelTest_MustReturnValidDbcModel()
        {
            // Act
            DbcModel actDbcModel = await _dbcLoader.LoadDbcModel(PathToExpTestModel);

            // Assert
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.Model, actDbcModel.Vehicle.Model);
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.EngineType, actDbcModel.Vehicle.EngineType);
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.TransmissionType, actDbcModel.Vehicle.TransmissionType);
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.ModelYear, actDbcModel.Vehicle.ModelYear);
            Assert.Equal(DbcFixture.TestDbcModel.DbcMessages.Count, actDbcModel.DbcMessages.Count);
        }

        [Fact]
        public async Task SaveDbcModelTest_MustReturnValidDbcModel()
        {
            // Act
            await _dbcLoader.SaveDbcModel(PathToActTestModel, DbcFixture.TestDbcModel);

            // Assert
            DbcModel actDbcModel = null;
            using (StreamReader file = File.OpenText(PathToActTestModel))
            {
                JsonSerializer serializer = new JsonSerializer();
                actDbcModel = (DbcModel)serializer.Deserialize(file, typeof(DbcModel));
            }

            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.Model, actDbcModel.Vehicle.Model);
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.EngineType, actDbcModel.Vehicle.EngineType);
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.TransmissionType, actDbcModel.Vehicle.TransmissionType);
            Assert.Equal(DbcFixture.TestDbcModel.Vehicle.ModelYear, actDbcModel.Vehicle.ModelYear);
            Assert.Equal(DbcFixture.TestDbcModel.DbcMessages.Count, actDbcModel.DbcMessages.Count);
        }
    }
}
